from telethon.sync import TelegramClient, events
from telethon.errors import SessionPasswordNeededError
from telethon.tl.types import PeerUser

import settings
from time import sleep
import paho.mqtt.client as mqtt
import logging
from threading import Thread


client = TelegramClient(f'sessions/{settings.phone.replace("+", "")}.session', settings.api_id, settings.api_hash)
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)

while not client.is_connected():
    sleep(1)
    client.connect()

if not client.is_user_authorized():
    client.send_code_request(settings.phone)
    try:
        myself = client.sign_in(settings.phone, input('Enter code: '))
    except SessionPasswordNeededError:
        client.sign_in(password=input('Enter cloud password: '))


mq = mqtt.Client("WeatherSayer service")
mq.connect('localhost')

process = Thread(target=mq.loop_forever)
process.daemon = True
process.start()


@client.on(event=events.NewMessage())
def messageHandler(event):
    if type(event.message.peer_id) == PeerUser and event.message.peer_id.user_id in [288946889, 888951733]:
        logger.info(f"weather received: {event.message.message}")
        msg_string = f'{event.message.message}'.replace("\n", "").strip()
        publish_mq(topic="/weather/today", payload=msg_string)
    else:
        logger.info(f"event received: {event}")


def publish_mq(topic, payload):
    try:
        mq.publish(topic=topic, payload=payload)
    except Exception as ex:
        logger.error(f'Error!: {ex}')


client.run_until_disconnected()
